﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {

    [SerializeField] private List<GameObject> _characters;
    [SerializeField] private List<GameObject> _respawnBoundaries;
    private Vector2 _bottomLeftBoundary;
    private Vector2 _topRightBoundary;

    private List<GameObject> _Players;

    public Transform _cam;
    [SerializeField] private float _camZoomOutDist;
    private Vector3 _camStartPosition;
    private int _currentStage;
    private float _interpolator = 9001f;
    [SerializeField] private float _rotateSpeed;

    [SerializeField] Transform _spawnHolder;
    Transform[] _spawnPoints;

    [SerializeField] Transform _playerHolder;
    public Transform[] _players;

    [SerializeField] private float _minSpawnDist;

    private bool check;
    private bool rotated;
   

	void Start ()
    {
        _Players = new List<GameObject>();
        check = false;
        rotated = false;
        Random.InitState((int)System.DateTime.Now.Ticks);

        //Get spawnpoints
        _spawnPoints = new Transform[_spawnHolder.childCount];
        for (int i = 0; i < _spawnHolder.childCount; i++)
        {
            _spawnPoints[i] = _spawnHolder.GetChild(i);
        }

        if (_respawnBoundaries.Count >= 2)
        {
            Vector2 bound1 = _respawnBoundaries[0].transform.position;
            Vector2 bound2 = _respawnBoundaries[1].transform.position;

            _bottomLeftBoundary = new Vector2(Mathf.Min(bound1.x, bound2.x), Mathf.Min(bound1.y, bound2.y));
            _topRightBoundary = new Vector2(Mathf.Max(bound1.x, bound2.x), Mathf.Max(bound1.y, bound2.y));
        }

        GameObject playerData = GameObject.Find("Playerdata");
        if (playerData)
        {
            PlayerData playerScript = playerData.GetComponent<PlayerData>();

            List<int> controllerIDs = playerScript._controllerIDs;
            List<int> characters = playerScript._selectedCharacters;

            for (int i = 0; i < controllerIDs.Count; i++)
            {
                print(i);
                GameObject character = Instantiate(_characters[characters[i] - 1], FindEmptySpawnPoint().position, Quaternion.identity);
                PlayerController playerController = character.GetComponent<PlayerController>();

                if (playerController)
                {
                    playerController.PlayerID = controllerIDs[i];
                    playerController.SetBoundaries(_bottomLeftBoundary, _topRightBoundary);
                }
                else
                {
                    PlayerControllerImproved pc = character.GetComponent<PlayerControllerImproved>();
                    pc.PlayerID = controllerIDs[i];
                    pc.SetBoundaries(_bottomLeftBoundary, _topRightBoundary);
                }

                playerScript._players.Add(character);
                _Players.Add(character);
            }
        }
        
        //_cam = Camera.main.transform;
        _camStartPosition = _cam.transform.position;


        GameObject canvas = GameObject.Find("Canvas");
        HealthManager hm = canvas.GetComponent<HealthManager>();
        hm.SendMessage("Init");
    }

    void Update()
    {
        int dead = 0;
        for (int i = 0; i < _Players.Count; ++i)
        {
            if (_Players[i].GetComponent<PlayerControllerImproved>()._livesLeft <= 0)
                ++dead;
        }

        if (dead >= _Players.Count - 1)
        {
            NextStage();
            for (int i = 0; i < _Players.Count; ++i)
            {
                _Players[i].GetComponent<PlayerControllerImproved>().Revive();
                _Players[i].SetActive(true);
            }
        }

        //Rotate level
        if (_interpolator < 1)
        {
            for (int i = 0; i < _Players.Count; ++i)
            {
                _Players[i].GetComponent<PlayerControllerImproved>().Respawn();
            }
            this.transform.rotation = Quaternion.Lerp(Quaternion.Euler(-90, (_currentStage * 90 - 90) * -1, 0), Quaternion.Euler(-90, (_currentStage * 90) * -1, 0), _interpolator) ;
            _interpolator += Time.deltaTime * _rotateSpeed;

            //Camera zoom out
            if (_interpolator < 0.5f)
            {
                _cam.transform.position = Vector3.Lerp(_camStartPosition, _camStartPosition - new Vector3(0, 0, _camZoomOutDist), _interpolator);
            }
            else
            {
                _cam.transform.position = Vector3.Lerp(_camStartPosition - new Vector3(0, 0, _camZoomOutDist), _camStartPosition, _interpolator);
            }
        }
        if (_interpolator >= 1 && rotated == false) rotated = true;

        // reset if pressed space
        if (check == true && rotated == true)
        {
            // get spawnpoints
            _spawnPoints = new Transform[_spawnHolder.childCount];
            for (int i = 0; i < _spawnHolder.childCount; i++)
            {
                _spawnPoints[i] = _spawnHolder.GetChild(i);
            }

            if (_respawnBoundaries.Count >= 2)
            {
                Vector2 bound1 = _respawnBoundaries[0].transform.position;
                Vector2 bound2 = _respawnBoundaries[1].transform.position;

                _bottomLeftBoundary = new Vector2(Mathf.Min(bound1.x, bound2.x), Mathf.Min(bound1.y, bound2.y));
                _topRightBoundary = new Vector2(Mathf.Max(bound1.x, bound2.x), Mathf.Max(bound1.y, bound2.y));
            }
            rotated = false;

            //GameObject playerData = GameObject.Find("Playerdata");
            //if (playerData)
            //{
            //    PlayerData playerScript = playerData.GetComponent<PlayerData>();
            //
            //    List<int> controllerIDs = playerScript._controllerIDs;
            //    List<int> characters = playerScript._selectedCharacters;
            //
            //    for (int i = 0; i < controllerIDs.Count; i++)
            //    {
            //        print(i);
            //        GameObject character = Instantiate(_characters[characters[i] - 1], FindEmptySpawnPoint().position, Quaternion.identity);
            //        PlayerController playerController = character.GetComponent<PlayerController>();
            //
            //        if (playerController)
            //        {
            //            playerController.PlayerID = controllerIDs[i];
            //            playerController.SetBoundaries(_bottomLeftBoundary, _topRightBoundary);
            //        }
            //        else
            //        {
            //            PlayerControllerImproved pc = character.GetComponent<PlayerControllerImproved>();
            //            pc.PlayerID = controllerIDs[i];
            //            pc.SetBoundaries(_bottomLeftBoundary, _topRightBoundary);
            //            pc.SetLives();
            //        }
            //        
            //    }
            //}
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            NextStage();
        }
    }

    public void NextStage()
    {
        check = true;

        _currentStage += 1;
        _interpolator = 0;
        

        
    }

    private Transform FindEmptySpawnPoint()
    {
        Transform spawnpoint = FindRandomSpawnPoint();

        while (CheckIfEmpty(spawnpoint) == false)
        {
            spawnpoint = FindRandomSpawnPoint();
        }

        return spawnpoint;
    }

    private Transform FindRandomSpawnPoint()
    {
        return _spawnPoints[Random.Range(0, _spawnPoints.Length)];
    }

    private bool CheckIfEmpty(Transform position)
    {
        for (int i = 0; i < _players.Length; i++)
        {
            if (Vector3.Distance(position.position, _players[i].position) < _minSpawnDist)
            {
                return false;
            }
        }

        return true;
    }
}
