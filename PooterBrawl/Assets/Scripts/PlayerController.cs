﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    private bool _isAlive = true;
    private float _health = 1.0f;
    public float _maxHealth = 1.0f;
    [SerializeField] private int _startingLives;
    private int _livesLeft;

    private Image _healthBar;

    private CharacterController _characterController;

    [SerializeField] private float _movementSpeed;
    [SerializeField] private float _jumpSpeed;
    [SerializeField] private float _dashSpeed;

    private Vector3 _moveDirection;

    private Vector3 _totalForce = Vector3.zero;
    private Vector3 _gravityForce = Vector3.zero;
    private Vector3 _dashForce = Vector3.zero;
    private Vector3 _moveInputForce = Vector3.zero;
    private Vector3 _knockbackForce = Vector3.zero;
    private Vector3 _jumpForce = Vector3.zero;

    [SerializeField] private float _amountOfJumps;
    private float _jumpsLeft;


    [SerializeField] private float _dashCooldownTime;
    private float _dashCooldownTimeLeft;

    private Animator _anim;
    private int _speedFloatAnimator;
    private int _groundedBool;
    private int _jumpBool;
    private int _doublejumpBool;
    private int _castSpellBool;
    private int _punchBool;
    public int PlayerID = 1;

    private float _punchRate = 0.2f;
    private float _nextPunch = 0.0f;
    private float _castRate = 0.2f;
    private float _nextCast = 0.0f;
    private bool _inJump = false;

    private Vector2 _bottomLeftBoundary;
    private Vector2 _topRightBoundary;

    [SerializeField] GameObject _ragdollCharacterPrefab;

    void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _livesLeft = _startingLives;
        _jumpsLeft = _amountOfJumps;
        _anim = GetComponent<Animator>();
        _speedFloatAnimator = Animator.StringToHash("Speed");
        _groundedBool = Animator.StringToHash("Grounded");
        _jumpBool = Animator.StringToHash("Jump");
        _anim.SetBool(_groundedBool, true);
        _doublejumpBool = Animator.StringToHash("DoubleJump");
        _castSpellBool = Animator.StringToHash("CastSpell");
        _punchBool = Animator.StringToHash("Punch");

        //_healthBar = transform.Find("Canvas").Find("EmptyHealth").Find("Health").GetComponent<Image>();

    }

    void FixedUpdate()
    {
        if (_isAlive)
        {
            Movement();
            Rotating();
        }
    }

    void Update()
    {
        //healthbar
        //_healthBar.fillAmount = _health / _maxHealth;

        //Input
        if (Input.GetButtonDown("CastSpell_P" + PlayerID) && Time.time > _nextCast)
        {
            _nextCast = Time.time + _castRate;
            _anim.SetTrigger(_castSpellBool);
            GetComponent<ProjectileHandlerQ>().Fire();
        }

        if (Input.GetButtonDown("Punch_P" + PlayerID) && Time.time > _nextPunch)
        {
            _nextPunch = Time.time + _punchRate;
            _anim.SetTrigger(_punchBool);
            GetComponent<ProjectileHandlerQ>().Grenade();
        }

        if (Input.GetButtonDown("Jump_P" + PlayerID) == true)
        {
            _inJump = true;
            Invoke("SetInJumpToFalse", 0.01f * Time.deltaTime);
            _jumpsLeft -= 1;
            Debug.Log(_jumpsLeft);
            if (_jumpsLeft > -1)
            {
                _jumpForce = transform.up * _jumpSpeed * Time.deltaTime;
                _gravityForce = Vector3.zero;
                _anim.SetTrigger(_jumpBool);
            }

            if (_jumpsLeft < 1)
            {
                _anim.SetTrigger(_doublejumpBool);
            }
        }
        
        //reset gravity
        if (!_inJump)
        {
            if (IsGroundedByRaycast() == true)
            {
                _jumpsLeft = _amountOfJumps;
                _gravityForce = Vector3.zero;
                //_gravityForce = -transform.up * 0.023f; //Not Vector3.zero because isGrounded is buggy otherwise
            }
        }

        if (Input.GetButtonDown("Dash_P" + PlayerID) == true)
        {
            if (_dashCooldownTimeLeft <= 0)
            {
                _dashForce += _moveInputForce * _dashSpeed * Time.deltaTime;
                _dashCooldownTimeLeft = _dashCooldownTime;
            }
        }

        _dashCooldownTimeLeft -= Time.deltaTime;
        _anim.SetBool(_groundedBool, IsGrounded());
    }

    private void SetInJumpToFalse()
    {
        _inJump = false;
    }

    private void Movement()
    {
        _totalForce = _moveInputForce + _dashForce + _gravityForce + _knockbackForce + _jumpForce;

        _gravityForce.y = Physics.gravity.y * Time.fixedDeltaTime; //Apply gravity

        //calculating movement direction
        _moveDirection = new Vector3(-Input.GetAxisRaw("Horizontal_P" + PlayerID), 0, 0);
        _moveInputForce += _moveDirection * _movementSpeed;
        _moveInputForce = Vector3.ClampMagnitude(_moveInputForce, _movementSpeed) * Time.fixedDeltaTime;

        //damp the force, you can tweak the 5 value for faster or slower damping
        _moveInputForce = Vector3.Lerp(_moveInputForce, Vector3.zero, 5.0f * Time.fixedDeltaTime);
        _dashForce = Vector3.Lerp(_dashForce, Vector3.zero, 5.0f * Time.fixedDeltaTime);
        _knockbackForce = Vector3.Lerp(_knockbackForce, Vector3.zero, 10.0f * Time.fixedDeltaTime);
        _jumpForce = Vector3.Lerp(_jumpForce, Vector3.zero, 10.0f * Time.fixedDeltaTime);

        //Move the controller by adding the force and the movement
        _characterController.Move(_totalForce);

        //Anim
        _anim.SetFloat(_speedFloatAnimator, _moveInputForce.magnitude * 1000, 0.1f, Time.fixedDeltaTime);

        //change the direction of the bullets
        if(_moveDirection.x > 0)
        {
            GetComponent<ProjectileHandlerQ>().SetDirection(true);
        }
        else if(_moveDirection.x <0)
        {
            GetComponent<ProjectileHandlerQ>().SetDirection(false);
        }

    }

    public void PerformKnockback(Vector3 direction, float knockbackforce, float damage)
    {
        direction.y = 0;
        _knockbackForce = direction * knockbackforce * Time.deltaTime;

        _health -= damage;
        if (_health <= 0)
        {
            GameObject ragdollClone = Instantiate(_ragdollCharacterPrefab, this.transform.position, this.transform.rotation);
            Rigidbody[] ragdollCloneRBs = ragdollClone.GetComponentsInChildren<Rigidbody>();
            foreach(Rigidbody rb in ragdollCloneRBs)
            {
                rb.AddForce(_knockbackForce/2, ForceMode.Impulse);
            }
            Destroy(this.gameObject);

            //Call method on ragdoll to transfer velocity

            //Death and respawn routine
        }
    }

    private void Rotating()
    {
        if (Input.GetAxisRaw("Horizontal_P" + PlayerID) < 0)
        {
            transform.transform.eulerAngles = new Vector3(0, 90, 0);
        }
        else if (Input.GetAxisRaw("Horizontal_P" + PlayerID) > 0)
        {
            transform.transform.eulerAngles = new Vector3(0, -90, 0);
        }
    }

    public bool IsGrounded()
    {
        return GetComponent<CharacterController>().isGrounded;
    }

    public bool IsGroundedByRaycast()
    {
        //Debug.DrawRay(transform.position, new Vector3(0f, -1f, 0f) * 0.1f, Color.green);       //draw the line to be seen in scene window

        RaycastHit hit;

        if (Physics.Raycast(transform.position, new Vector3(0f, -1f, 0f), out hit, 0.1f))
        {      //if we hit something
            return true;
        }
        return false;
    }

    public void Kill()
    {
        --_livesLeft;
        if (_livesLeft > 0)
            Respawn();
    }

    void Respawn()
    {
        float x = Random.Range(_bottomLeftBoundary.x, _topRightBoundary.x);
        float y = Random.Range(_bottomLeftBoundary.y, _topRightBoundary.y);

        this.transform.position = new Vector3(x, y, this.transform.position.z);
    }

    public void SetBoundaries(Vector2 bottomLeft, Vector2 topRight)
    {
        _bottomLeftBoundary = bottomLeft;
        _topRightBoundary = topRight;
    }
}
