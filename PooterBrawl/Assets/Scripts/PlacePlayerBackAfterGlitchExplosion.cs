﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacePlayerBackAfterGlitchExplosion : MonoBehaviour {

     void Update()
    {
        if (transform.position.y > 12.0f)
        {
            GetComponent<PlayerControllerImproved>().Jump();
        }
    }
}
