﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waterfall : MonoBehaviour {

    private AudioSource _audioSrc;
    [SerializeField] private AudioClip _plonsSound;

	void Start ()
    {
        _audioSrc = this.gameObject.GetComponent<AudioSource>();
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _audioSrc.PlayOneShot(_plonsSound, 1.0f);
        }
    }
}
