﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpThroughPlatforms : MonoBehaviour {

    public bool topCollision = true;
    private float _cosine;
    public float MaxAngle = 45.0f;
    private MeshCollider _col;

    void Start()
    {
        _cosine = Mathf.Cos(MaxAngle);
        MeshCollider _col = GetComponent<MeshCollider>();

        if (_col == null)
        {
            Debug.LogError("MeshCollider not found");
            return;
        }

        Mesh mesh = new Mesh();
        Vector3[] verts = _col.sharedMesh.vertices;
        List<int> triangles = new List<int>(_col.sharedMesh.triangles);
        for (int i = triangles.Count - 1; i >= 0; i -= 3)
        {
            Vector3 P1 = transform.TransformPoint(verts[triangles[i - 2]]);
            Vector3 P2 = transform.TransformPoint(verts[triangles[i - 1]]);
            Vector3 P3 = transform.TransformPoint(verts[triangles[i]]);
            Vector3 faceNormal = Vector3.Cross(P3 - P2, P1 - P2).normalized;
            if ((topCollision && Vector3.Dot(faceNormal, Vector3.up) <= _cosine) ||
                 (!topCollision && Vector3.Dot(faceNormal, -Vector3.up) <= _cosine))
            {
                triangles.RemoveAt(i);
                triangles.RemoveAt(i - 1);
                triangles.RemoveAt(i - 2);
            }
        }
        mesh.vertices = verts;
        mesh.triangles = triangles.ToArray();
        _col.sharedMesh = mesh;
    }
}
