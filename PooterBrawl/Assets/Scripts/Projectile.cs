﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    private Vector3 _direction;
    [SerializeField] private float _speed;
    [SerializeField] private float _damage;
    [SerializeField] private float _knockbackForce;

    public GameObject HitEffect;
    private bool _test = false;

    private void Update()
    {
        if(_test)
        {
            gameObject.GetComponent<Rigidbody>().AddForce(_direction * _speed);
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("Player") == true)
    //    {
    //        PlayerController pc = other.GetComponent<PlayerController>();
    //        pc.PerformKnockback(_direction, _knockbackForce, _damage);
    //    }

    //    Destroy(gameObject);


    //}
    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(HitEffect, transform.position, transform.rotation);
        Destroy(gameObject);
        _direction.y = 0;
        if (collision.collider.CompareTag("Player"))
        {
            PlayerController pc = collision.collider.GetComponent<PlayerController>();
            Vector3 holder = new Vector3(_direction.x,0,0);

            if (pc)
                pc.PerformKnockback(holder, _knockbackForce, _damage);
            else
            {
                PlayerControllerImproved pci = collision.collider.GetComponent<PlayerControllerImproved>();
                pci.PerformKnockback(holder, _knockbackForce, _damage);
            }
        }
        else
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(_direction.x * _knockbackForce, 0.0f, 0.0f);
        }
    }

    public void SetDirection(Vector3 direction)
    {
        _direction = direction;
        _test = true;
    }
}
