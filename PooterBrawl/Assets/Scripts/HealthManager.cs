﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

    private List<GameObject> _playerList;
    private List<GameObject> _healthList;
    private bool hasInited = false;

    // Use this for initialization
    void Start()
    {
    }
	
	// Update is called once per frame
	void Update () {
		
        if (hasInited)
        {
            for (int i = 0; i < _playerList.Count; ++i)
            {

                PlayerControllerImproved pci = _playerList[i].GetComponent<PlayerControllerImproved>();

                float hp = pci._health;

                GameObject healthBar = this.transform.GetChild(i+1).gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject;
                healthBar.GetComponent<Image>().fillAmount = (pci._health / pci._maxHealth);

                GameObject life1 = this.transform.GetChild(i + 1).gameObject.transform.GetChild(1).gameObject.transform.GetChild(0).gameObject;
                GameObject life2 = this.transform.GetChild(i + 1).gameObject.transform.GetChild(2).gameObject.transform.GetChild(0).gameObject;
                GameObject life3 = this.transform.GetChild(i + 1).gameObject.transform.GetChild(3).gameObject.transform.GetChild(0).gameObject;

                if (pci._livesLeft <= 2)
                    life3.GetComponent<Image>().fillAmount = 0;
                else
                    life3.GetComponent<Image>().fillAmount = 1;

                if (pci._livesLeft <= 1)
                    life2.GetComponent<Image>().fillAmount = 0;
                else
                    life2.GetComponent<Image>().fillAmount = 1;

                if (pci._livesLeft <= 0)
                    life1.GetComponent<Image>().fillAmount = 0;
                else
                    life1.GetComponent<Image>().fillAmount = 1;
            }
        }

	}

    public void Init()
    {
        print("I HAS INITED");
        GameObject playerData = GameObject.Find("Playerdata");

        if (playerData)
        {
            PlayerData playerScript = playerData.GetComponent<PlayerData>();

            List<int> characters = playerScript._selectedCharacters;
            _playerList = playerScript._players;

            for (int i = 1; i <= characters.Count; ++i)
            {
                this.gameObject.transform.GetChild(i).gameObject.SetActive(true);

                //_healthList.Add(GameObject.Find("UI_Player_" + i));
            }
        }

        hasInited = true;
    }
}
