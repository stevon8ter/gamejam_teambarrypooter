﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnStart : MonoBehaviour {

    private AudioSource _audioSrc;
    [SerializeField] private AudioClip _sound;

    // Use this for initialization
    void Start () {
        _audioSrc = this.gameObject.GetComponent<AudioSource>();
        _audioSrc.PlayOneShot(_sound, 1.0f);
    }

}
