﻿Shader "ScreenspaceGradient" {
Properties
{
	_FirstColor ("First", Color) = (1,1,1,1)
	_SecondColor ("Second", Color) = (1,1,1,1)
	_MainTex ("Base (RGB)", 2D) = "white" {}
}
SubShader
{
	Tags { "RenderType"="Opaque" }
	LOD 200

CGPROGRAM
#pragma surface surf Lambert

sampler2D _MainTex;
fixed4 _FirstColor;
fixed4 _SecondColor;

struct Input
{
	float2 uv_MainTex;
	float4 screenPos;
};

void surf (Input IN, inout SurfaceOutput o) {
	float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * lerp(_SecondColor,_FirstColor,screenUV.y);
	o.Albedo = c.rgb;
	o.Alpha = c.a;
}
ENDCG
}

Fallback "VertexLit"
}
