﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour {

    public List<int> _controllerIDs;
    public List<int> _selectedCharacters;
    public List<GameObject> _players;

	// Use this for initialization
	void Start () {
        _controllerIDs = new List<int>();
        _selectedCharacters = new List<int>();
	}

    void Awake()
    {
        DontDestroyOnLoad(this);
        // Do not destroy this game object:
        
    }

    // Update is called once per frame
    void Update () {
		
	}
}
