﻿using UnityEngine;
using System.Collections;

public class PlayerControllerImproved : MonoBehaviour
{
    private Rigidbody _rb;
    private CharacterController _cc;
    public GameObject _ragdollCharacterPrefab;

    public int PlayerID = 0;
    public float _movespeed = 10.0f;
    public float _jumpHeight = 4.0f;
    public float _health = 100.0f;
    public float _maxHealth = 1.0f;
    public int StartingLives = 3;
    public int _livesLeft;

    private bool _grounded = false;
    private int _maxJump = 2;
    private int _currentJump = 0;
    public float DashCooldownTime = 2.0f;
    private float _dashCooldownTimeLeft;
    public float DashSpeed = 3.0f;
    private float _punchRate = 0.5f;
    private float _nextPunch = 0.0f;
    private float _castRate = 0.3f;
    private float _nextCast = 0.0f;

    private Animator _anim;
    private int _speedFloatAnimator;
    private int _groundedBool;
    private int _jumpBool;
    private int _doublejumpBool;
    private int _castSpellBool;
    private int _punchBool;
    private Vector3 _knockbackForce = Vector3.zero;

    private Vector2 _bottomLeftBoundary;
    private Vector2 _topRightBoundary;

    private Vector3 _movementDirection;
    private GameObject[] _characterColliders;
    private bool _doubleJumpNow = false;

    [SerializeField] private AudioSource _audioSrc;
    [SerializeField] private AudioClip _jumpSound;
    [SerializeField] private AudioClip _spellSound;
    [SerializeField] private AudioClip _punchSound;
    [SerializeField] private AudioClip _deathSound;

    void Start()
    {
        //_rb = GetComponent<Rigidbody>();

        _anim = GetComponent<Animator>();
        _speedFloatAnimator = Animator.StringToHash("Speed");
        _groundedBool = Animator.StringToHash("Grounded");
        _jumpBool = Animator.StringToHash("Jump");
        _anim.SetBool(_groundedBool, true);
        _doublejumpBool = Animator.StringToHash("DoubleJump");
        _castSpellBool = Animator.StringToHash("CastSpell");
        _punchBool = Animator.StringToHash("Punch");

        _characterColliders = GameObject.FindGameObjectsWithTag("Player");
        _cc = GetComponent<CharacterController>();

        _livesLeft = StartingLives;
    }

    private void Update()
    {
        //ignore colliding with players
        for (int i = 0; i < _characterColliders.Length; i++)
        {
            Physics.IgnoreCollision(GetComponent<Collider>(), _characterColliders[i].GetComponent<Collider>());
            i += 1;
        }

        Movement();
        Jump();
        Rotating();

        if (Input.GetButtonDown("Dash_P" + PlayerID) == true)
        {
            //Dash();
        }

        if (Input.GetButtonDown("Punch_P" + PlayerID) && Time.time >= _nextPunch)
        {
            Punch();
        }

        if (Input.GetButtonDown("CastSpell_P" + PlayerID) && Time.time >= _nextCast)
        {
            CastSpell();
        }

        _dashCooldownTimeLeft -= Time.deltaTime;
        _anim.SetBool(_groundedBool, _cc.isGrounded);
    }

    private void Movement()
    {
        float gravity = 3 * 9.81f;

        float xChange = -Input.GetAxis("Horizontal_P" + PlayerID) * _movespeed;
        _movementDirection.x += xChange;

        _movementDirection.x = Mathf.Clamp(_movementDirection.x, -_movespeed, _movespeed);

        if (_movementDirection.x > 0.01f)
            _movementDirection.x -= 20 * Time.deltaTime;
        else if (_movementDirection.x < -0.01f)
            _movementDirection.x += 20 * Time.deltaTime;

        _movementDirection.y -= gravity * Time.deltaTime;

        if (_cc.isGrounded)
        {
            print("GROUNDED");
            //_movementDirection.y = 0;
            _currentJump = 0;
            _anim.SetFloat(_speedFloatAnimator, Mathf.Abs(xChange), 0.1f, Time.deltaTime);
        }

        _cc.Move(_movementDirection * Time.deltaTime);
    }

    public void Jump()
    {
        if (Input.GetButtonDown("Jump_P" + PlayerID) && (_cc.isGrounded || _maxJump > _currentJump))
        {
            _grounded = false;
            _movementDirection.y = _jumpHeight;

            if (_doubleJumpNow)
            {
                //animation doublejump
                _anim.SetTrigger(_doublejumpBool);
                _doubleJumpNow = false;
            }
            else
            {
                //animation jump
                _anim.SetTrigger(_jumpBool);
                _doubleJumpNow = true;
            }
            ++_currentJump;
        }
    }

    //private void Dash()
    //{
    //    if (_dashCooldownTimeLeft <= 0)
    //    {
    //        print("dash");
    //        PerformKnockback(_movementDirection, DashSpeed, 0);
    //        _dashCooldownTimeLeft = DashCooldownTime;
    //    }
    //}

    private void Punch()
    {
        _nextPunch = Time.time + _punchRate;
        _anim.SetTrigger(_punchBool);
        GetComponent<ProjectileHandlerQ>().Grenade();
    }

    private void CastSpell()
    {
        _audioSrc.PlayOneShot(_spellSound, 1.0f);
        _nextCast = Time.time + _castRate;
        _anim.SetTrigger(_castSpellBool);
        GetComponent<ProjectileHandlerQ>().Fire();
    }

    private void Rotating()
    {
        if (Input.GetAxisRaw("Horizontal_P" + PlayerID) > 0)
        {
            transform.transform.eulerAngles = new Vector3(0, -90, 0);

            //change direction of bullets
            GetComponent<ProjectileHandlerQ>().SetDirection(false);
        }
        else if (Input.GetAxisRaw("Horizontal_P" + PlayerID) < 0)
        {
            //change direction of bullets
            GetComponent<ProjectileHandlerQ>().SetDirection(true);

            transform.transform.eulerAngles = new Vector3(0, 90, 0);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Floor"))
        {
            _grounded = true;
            _currentJump = 0;
        }
    }

    public void PerformKnockback(Vector3 direction, float knockbackforce, float damage)
    {
        _audioSrc.PlayOneShot(_punchSound, 1.0f);
        //direction.y = 0;
        Vector3 knock = direction.normalized * knockbackforce;

        _health -= damage;
        if (_health <= 0)
        {
            //GameObject ragdollClone = Instantiate(_ragdollCharacterPrefab, this.transform.position, this.transform.rotation);
            //Rigidbody[] ragdollCloneRBs = ragdollClone.GetComponentsInChildren<Rigidbody>();
            //foreach (Rigidbody rb in ragdollCloneRBs)
            //{
            //    rb.AddForce(knock, ForceMode.Impulse);
            //}
            this.Kill();
        }
        else
        {
            //print("knock: " + knock);
            //_rb.AddForce(knock * 100, ForceMode.Acceleration);
            //_rb.velocity += direction * knockbackforce;
            _movementDirection = knock;
        }
    }

    public void Kill()
    {
        _audioSrc.PlayOneShot(_deathSound, 1.0f);
        print("kill" + _livesLeft);
        --_livesLeft;
        if (_livesLeft > 0)
            Respawn();
        else
            this.gameObject.SetActive(false);
    }

    public void SetLives()
    {
        _livesLeft = 3;
        Respawn();

    }

    public void Respawn()
    {
        _health = _maxHealth;
        _movementDirection = Vector3.zero;

        print("respawning" + _health);

        float x = Random.Range(_bottomLeftBoundary.x, _topRightBoundary.x);
        float y = Random.Range(_bottomLeftBoundary.y, _topRightBoundary.y);

        this.transform.position = new Vector3(x, y, this.transform.position.z);
    }

    public void SetBoundaries(Vector2 bottomLeft, Vector2 topRight)
    {
        _bottomLeftBoundary = bottomLeft;
        _topRightBoundary = topRight;
    }

    public void Revive()
    {
        _livesLeft = 3;
        Respawn();
    }
}