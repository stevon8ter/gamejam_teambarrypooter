﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchQ : MonoBehaviour {

    private Vector3 _PunchPos;
    private bool _DirectionRight = false;

    public Vector3 _punchScale;
    public float _Power;
    public float _Damage = 10;
    public float _ThrowingForce = 100;
    public GameObject HitEffect;

    public float ResetTime = 1.0f;
    private float Timer = 2.0f;
    private bool check;

    private Vector3 _Direction = new Vector3(0,0,0);

    // Use this for initialization
    void Start () {
        Timer = ResetTime;
        check = true;
	}
	
	// Update is called once per frame
	void Update () {


           if (Input.GetKeyDown(KeyCode.S))
           {
               Punch();

           }
        
        if (Input.GetKeyDown(KeyCode.RightArrow)) _DirectionRight = true;
        if (Input.GetKeyDown(KeyCode.LeftArrow)) _DirectionRight = false;

        if (Timer <= 0)
        {
            Timer = ResetTime;
            check = false;
            Debug.Log("Test");
        }
        if (Timer >= 0 && check)
        {
            Timer -= Time.deltaTime;
        }
    }

    private void Punch()
    {
        if (Timer >= ResetTime)
        {
            check = true;
            Rigidbody ownRb = gameObject.GetComponent<Rigidbody>();
            //GameObject Cube = GameObject.CreatePrimitive(PrimitiveType.Cube);

            if (_DirectionRight)
            {
                _PunchPos.x = transform.position.x + (transform.localScale.x / 2) + (_punchScale.x / 2) + 0.5f;
                _Direction.x = 1;
            }
            else
            {
                _PunchPos.x = transform.position.x - (transform.localScale.x / 2) - (_punchScale.x / 2) - 0.5f;
                _Direction.x = -1;

            }

            _PunchPos.y = (transform.position.y + (transform.localScale.y / 2)) + (_punchScale.y / 2); // devide by 2 or dont it only moves it a litle highter
            _PunchPos.z = transform.position.z;

            //Cube.transform.position = _PunchPos;
            //Cube.transform.localScale = _punchScale;

            Collider[] punchHitbox = Physics.OverlapBox(_PunchPos, _punchScale);

            foreach (Collider nearbyObject in punchHitbox)
            {
                Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();

                //if (!(rb == ownRb))
                //{

                if (nearbyObject.CompareTag("Player") == true)
                {
                    PlayerController pc = nearbyObject.GetComponent<PlayerController>();
                    pc.PerformKnockback(_Direction, _ThrowingForce, _Damage);

                }
                else
                {

                    if (rb != null)
                    {
                        rb.AddForce(_Direction * _ThrowingForce);


                    }
                }
                Instantiate(HitEffect, nearbyObject.transform.position, nearbyObject.transform.rotation);
                //}

            }
        }
        
    }

    public void SetDirection(bool movingRight)
    {
        _DirectionRight = movingRight;
    }
}
