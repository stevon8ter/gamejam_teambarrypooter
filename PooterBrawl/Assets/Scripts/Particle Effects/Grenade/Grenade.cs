﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{

    public float radius = 0.002f;
    public float force = 0.02f;

    public GameObject explosionEffect;

    public float _ThrowingForce;
    public Vector3 _Direction;
    public float _Damage;

    private Vector3 _throwingVector;



    // Use this for initialization
    void Start()
    {
        _throwingVector = new Vector3(_ThrowingForce * _Direction.x, 1000.0f, 0.0f);
        gameObject.GetComponent<Rigidbody>().AddForce(_throwingVector);

    }

    // Update is called once per frame
    void Update()
    {
        
    }



    void Explode()
    {

        // show effect
        Instantiate(explosionEffect, transform.position, transform.rotation);

        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce((force * 100) / 2, transform.position, radius);
            }
            if (nearbyObject.CompareTag("Player") == true)
            {
                PlayerController pc = nearbyObject.GetComponent<PlayerController>();
                PlayerControllerImproved pci = nearbyObject.GetComponent<PlayerControllerImproved>();
                Vector3 explodeDirection = gameObject.GetComponent<Rigidbody>().position;

                Vector3 PlayerPos;

                if (pc)
                {
                    PlayerPos = pc.GetComponent<Transform>().position;
                }
                else
                {
                    PlayerPos = pci.GetComponent<Transform>().position;
                }

                Vector3 Direction = (PlayerPos - explodeDirection);
                Direction.Normalize();
                if (pc)
                    pc.PerformKnockback(Vector3.zero, 0, _Damage);
                else
                    pci.PerformKnockback(Vector3.zero, 0, _Damage);

            }
        }
        // get nearby objects
        // add force
        // damage


        // remove grenade
        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        Explode();
    }

    public void SetDirection(Vector3 direction)
    {
        _Direction = direction;
    }

}
