﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePlayerQ : MonoBehaviour
{

    private Vector3 _knockbackForce = Vector3.zero;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PerformKnockback(Vector3 direction, float knockbackforce)
    {
        direction.y = 0;
        _knockbackForce = direction * knockbackforce;

        gameObject.GetComponent<Rigidbody>().AddForce(_knockbackForce);

    }
}
