﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileHandlerQ : MonoBehaviour {

    //public Transform spawnLocation;
    public GameObject ProjectilePrefab;
    public GameObject GrenadePrefab;
    private GameObject newProjectile;
    private GameObject newGrenade;

    private Vector3 PlayerSpawnLocation;
    public bool RightSide = true;
    private float _Direction;
    private Vector3 _BulletDirection;
    private Vector3 _GrenadeDirection;
    private float _GrenadeTimer = 2;
    private float _ProjectileTimer = 1;
    private bool check = false;
    private bool check2 = false;

    public float ProjectileTimerReset = 1;
    public float GrenadeTimeReset = 2;

    public float _projectilePlacement = 1.0f;
    // Use this for initialization
    void Start() {

    }

    public void SetDirection(bool movingRight)
    {
        RightSide = movingRight;
        //GetComponent<PunchQ>().SetDirection(movingRight);
    }


	// Update is called once per frame
	void Update () {
        if(RightSide)
        {
            PlayerSpawnLocation.x = transform.position.x + (transform.localScale.x / 2) + _projectilePlacement;
            //_Direction = 1;
            _GrenadeDirection = new Vector3(1, 0, 0);
            _BulletDirection = new Vector3(1, 0, 0);

        }
        else if (!RightSide)
        {
            PlayerSpawnLocation.x = transform.position.x - (transform.localScale.x / 2) - _projectilePlacement;
            //_Direction = -1;
            _GrenadeDirection = new Vector3(-1, 0, 0);
            _BulletDirection = new Vector3(-1, 0, 0);

        }


        PlayerSpawnLocation.y = (transform.position.y + (transform.localScale.y)); //uncomment for in player script
        PlayerSpawnLocation.z = transform.position.z;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
        }
        if(Input.GetKeyDown(KeyCode.F))
        {
            Grenade();
        }

        if (_GrenadeTimer < 0)
        {
            _GrenadeTimer = GrenadeTimeReset;
            check = false;
            Debug.Log("Test");
        }
        if (_GrenadeTimer >= 0 && check)
        {
            _GrenadeTimer -= Time.deltaTime;
        }

        if (_ProjectileTimer < 0)
        {
            _ProjectileTimer = ProjectileTimerReset;
            check2 = false;
            Debug.Log("Test");
        }
        if (_ProjectileTimer >= 0 && check2)
        {
            _ProjectileTimer -= Time.deltaTime;
        }
    }

    public void Fire() // spawn the projectile
    {
        //Debug.Log(_BulletDirection.z);
        if (_ProjectileTimer >= ProjectileTimerReset)
        {
            check2 = true;
            newProjectile = Instantiate(ProjectilePrefab, PlayerSpawnLocation, transform.rotation) as GameObject;
            newProjectile.GetComponent<Projectile>().SetDirection(_BulletDirection);
            Physics.IgnoreCollision(newProjectile.GetComponent<Collider>(), GetComponent<Collider>());
        }

        Destroy(newProjectile, 1.0f);

    }

    public void Grenade()
    {
        
        if (_GrenadeTimer >= GrenadeTimeReset)
        {
            check = true;
            newGrenade = Instantiate(GrenadePrefab, PlayerSpawnLocation, transform.rotation) as GameObject;
            Grenade script = newGrenade.GetComponent<Grenade>();
            script.SetDirection(_GrenadeDirection);
            //var Grenade = (GameObject)Instantiate(GrenadePrefab, PlayerSpawnLocation, transform.rotation);
            
        }



        Destroy(newGrenade, 5.0f);
    }

   
}
