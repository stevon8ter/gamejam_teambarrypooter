﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour {

    EventSystem m_EventSystem;

    private List<int> _players;
    private List<GameObject> _playerIndicators;
    private List<bool> _hasMovedList;
    private List<int> _selectedCharacters;
    private List<float> _startingX;
    private List<bool> _locked;
    private List<int> _openCharacters;
    private int _lockedPlayers = 0;

    // Use this for initialization
    void Start () {
        m_EventSystem = EventSystem.current;

        _players = new List<int>();
        _playerIndicators = new List<GameObject>();
        _hasMovedList = new List<bool>();
        _selectedCharacters = new List<int>();
        _startingX = new List<float>();
        _locked = new List<bool>();
        _openCharacters = new List<int>();

        for (int i = 1; i <= 4; i++)
        {
            _playerIndicators.Add(GameObject.Find("Indicator_P" + i));
            _playerIndicators[i - 1].SetActive(false);
            _startingX.Add(_playerIndicators[i - 1].transform.localPosition.x);

            _openCharacters.Add(i);
        }
    }
	
	// Update is called once per frame
	void Update () {

        for (int i = 1; i <= Input.GetJoystickNames().Length; i++)
        {
            int index = _players.IndexOf(i);
            if (index == -1)
            {
                if (Input.GetButtonDown("Submit_P" + i))
                {
                    _players.Add(i);
                    _playerIndicators[_players.Count - 1].SetActive(true);

                    _selectedCharacters.Add(_openCharacters[0]);
                    _hasMovedList.Add(false);
                    _locked.Add(false);
                }
            }
            else
            {
                int character = _openCharacters.IndexOf(_selectedCharacters[index]);
                if (character == -1)
                    character = 0;

                if (Input.GetAxisRaw("Horizontal_P" + i) > 0 && !_hasMovedList[index])
                {
                    _hasMovedList[index] = true;

                    ++character;
                }
                else if (Input.GetAxisRaw("Horizontal_P" + i) < 0 && !_hasMovedList[index])
                {
                    _hasMovedList[index] = true;

                    --character;
                }
                else if (Input.GetAxisRaw("Horizontal_P" + i) == 0 && !_locked[index])
                {
                    _hasMovedList[index] = false;
                }

                if (Input.GetButtonDown("Submit_P" + i) && !_locked[index])
                {
                    _locked[index] = true;
                    _hasMovedList[index] = true;
                    _openCharacters.Remove(_selectedCharacters[index]);

                    GameObject door = GameObject.Find("Door_" + _selectedCharacters[index]);

                    StartCoroutine(AnimateMove(door, door.transform.localPosition, door.transform.localPosition - new Vector3(0, 31, 0), 1f, _playerIndicators[index]));
                }

                if (character < 0)
                    character = _openCharacters.Count - 1;
                if (character >= _openCharacters.Count)
                    character = 0;

                if (!_locked[index])
                    _selectedCharacters[index] = _openCharacters[character];

                Vector3 pos = _playerIndicators[index].transform.localPosition;
                _playerIndicators[index].transform.localPosition = new Vector3(_startingX[index] + (385 * (_selectedCharacters[index] - 1)), pos.y, pos.z);
                
            }
        }
    }

    IEnumerator AnimateMove(GameObject obj, Vector3 origin, Vector3 target, float duration, GameObject ind)
    {
        float journey = 0f;
        while (journey <= duration)
        {
            journey = journey + Time.deltaTime;
            float percent = Mathf.Clamp01(journey / duration);

            obj.transform.localPosition = Vector3.Lerp(origin, target, percent);

            yield return null;
        }
        obj.GetComponent<Renderer>().material.color = ind.GetComponent<Renderer>().material.color;
        ind.SetActive(false);

        ++_lockedPlayers;

        if (_lockedPlayers == _players.Count && _players.Count >= 2)
        {
            GameObject playerData = GameObject.Find("Playerdata");
            PlayerData playerScript = playerData.GetComponent<PlayerData>();

            playerScript._controllerIDs = _players;
            playerScript._selectedCharacters = _selectedCharacters;

            SceneManager.LoadScene("VisualsTestLevel");
        }
    }
}
